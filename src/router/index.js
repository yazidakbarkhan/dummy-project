//import vue router
import { createRouter, createWebHistory } from 'vue-router'


//define a routes
const routes = [
    {
        path: '/',
        name: 'home',
        component: () =>
            import ( /* webpackChunkName: "Home" */
                '@/views/Home.vue')
    },
    {
        path: '/profile',
        name: 'profile',
        component: () =>
            import ( /* webpackChunkName: "Profile" */
                '@/views/Profile.vue')
    },
    {
        path: '/datatable',
        name: 'datatable',
        component: () =>
            import ( /* webpackChunkName: "Datatable" */
                '@/views/Datatable.vue')
    },
    {
        path: '/posts',
        name: 'posts',
        component: () =>
            import ( /* webpackChunkName: "Datatable" */
                '@/views/Posts.vue')
    },
    {
        path: '/datatablecreate',
        name: 'datatablecreate',
        component: () =>
            import ( /* webpackChunkName: "Datatable Create" */
                '@/views/DatatableCreate.vue')
    },
    {
        path: '/datatableedit/:id',
        name: 'datatableedit',
        component: () =>
            import ( /* webpackChunkName: "Datatable Edit" */
                '@/views/DatatableEdit.vue')
    }
    
]

//create router
const router = createRouter({
    history: createWebHistory(),
    routes // <-- routes,
})

export default router